import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
# plt.rc('text', usetex=True)
import scipy.integrate as integrate

# Alex's project
def get_iR_corrected_voltage(U, i, minus_Im_Z, Re_Z):
    assert U.shape == i.shape
    index = np.argmax(minus_Im_Z[minus_Im_Z < 0])  # Returns the index of largest element below zero.
    U = np.array(U)
    i = np.array(i)
    minus_Im_Z = np.array(minus_Im_Z)
    Re_Z = np.array(Re_Z)
    # R = area unspecific resistance or area specific resistance if i is current density.
    R = Re_Z[index] + (Re_Z[index+1]-Re_Z[index]) * abs(minus_Im_Z[index])/(abs(minus_Im_Z[index])+minus_Im_Z[index+1])
    return U - i * R

# Alex's project
def get_coulombic_efficiency(I, CO2ppm):  # Units of should be A
    F = 96485 # units: C/mol
    flowRate = CO2ppm / (1000000+CO2ppm) * 0.0003745  # 0.0003745 mol/s is flow rate of H2, assumption: 1bar # units: mol/s
    return flowRate * F / I

# Alex's project
def get_energy_consumption(I, U, CO2ppm):
    flowRate = CO2ppm / (1000000+CO2ppm) * 0.0003745  # 0.0003745 mol/s is flow rate of H2, assumption: 1bar # units: mol/s
    return U * I / flowRate / 1000 # Units: kJ/mol

# Master Thesis, performs a linear fit
def get_HFR(minus_Im_Z, Re_Z):
    minus_Im_Z = minus_Im_Z[Re_Z < 0.25]
    Re_Z = Re_Z[Re_Z < 0.25] # Sometimes there are loops in impedance spectra, so cut off high Re(Z) values.
    index = np.argmax(minus_Im_Z[minus_Im_Z < 0])  # Returns the index of largest element below zero.
    minus_Im_Z = np.array(minus_Im_Z)
    Re_Z = np.array(Re_Z)
    # R = area unspecific resistance
    R = Re_Z[index] + (Re_Z[index+1]-Re_Z[index]) * abs(minus_Im_Z[index])/(abs(minus_Im_Z[index])+abs(minus_Im_Z[index+1]))
    return R

# Master Thesis
def get_last_imp_cycle(freq):
    freq = np.array(freq)
    index = -1
    while freq[index] < freq[index-1]:
        index -= 1
    return index

# Master Thesis
def get_H2crossover(current, voltage, startVol=0.5, endVol=0.3): # current can also be replaced with current density, in that case the return value is also a current density.
    # Assume that H2 crossover was sweeped from high to low voltage.
    startIndex = np.argmin(abs(voltage-startVol))
    EndIndex = np.argmin(abs(voltage-endVol))
    return np.mean(current[startIndex:EndIndex])

# Master Thesis
def get_ECSA_axis(CellVoltage, Current_density, HFR=0, H2crossover=0):
    CellVoltage = np.array(CellVoltage)
    Current_density = np.array(Current_density)
    assert CellVoltage.shape == Current_density.shape
    HighestPotIndex = np.argmax(CellVoltage)
    LowestPotIndex = np.argmin(CellVoltage)
    # Just for test dataset, remove later:
    #LowestPotIndex = 1999
    ReductiveSweepPotential = CellVoltage[HighestPotIndex:LowestPotIndex]
    ReductiveSweepCurrent = Current_density[HighestPotIndex:LowestPotIndex]
    ReductiveSweepPotential += ReductiveSweepCurrent * HFR / 1000 # Divide by 1000 bec. current in mA instead of A.
    # ReductiveSweepCurrent -= H2crossover
    OxidativeSweepPotential = np.concatenate([CellVoltage[LowestPotIndex:], CellVoltage[:HighestPotIndex]])
    OxidativeSweepCurrent = np.concatenate([Current_density[LowestPotIndex:], Current_density[:HighestPotIndex]])
    OxidativeSweepPotential += OxidativeSweepCurrent * HFR / 1000
    # OxidativeSweepCurrent += H2crossover
    return OxidativeSweepPotential, OxidativeSweepCurrent, ReductiveSweepPotential, ReductiveSweepCurrent

# Master Thesis (open file only for read)
def open_mpt(filename, encoding):
    file = open(filename, 'r')
    line0 = file.readline()
    line1 = file.readline()
    skipLines = int(line1[18:])
    # print(skipLines)
    file.close()
    return pd.read_csv(filename, delimiter='\t', skiprows=skipLines-1, encoding=encoding)

# Master Thesis (for obtaining activities at 0.9 V
def get_900mV(iRcorrVol, activities):
    assert iRcorrVol.shape == activities.shape
    index = np.argmin(iRcorrVol[iRcorrVol > 0.9])
    activities = np.array(activities)
    #print(activities)
    #print(iRcorrVol)
    #print(index)
    activities = np.log(activities)
    NineHundredmV_activity = activities[index+1] + (activities[index]-activities[index+1]) * (0.9 - iRcorrVol[index+1])/(iRcorrVol[index]-iRcorrVol[index+1])
    #print(np.exp(NineHundredmV_activity))
    return np.exp(NineHundredmV_activity)
    
