import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import functions as ele
import sys
UseTXT = True
UseMPT = False


CVFile = "ECSA after conditioning_GDE_HP_Pt3Ni_124_02_CVA_C01.txt"
CVFile = "ECSA before conditioning_GDE_HP_Pt3Ni_124_02_CVA_C01.txt"
#CVFile = "ECSA after RH measurements_GDE_HP_Pt3Ni_124_02_CVA_C01.mpt"
if UseTXT:
    CVData = pd.read_csv(CVFile, delimiter='\t', skiprows=[])
elif UseMPT:
    CVData = ele.open_mpt(CVFile, encoding = "iso8859-1")
#plt.title("CV after Conditioning")
plt.xlabel("Potential [V]")
plt.ylabel("Current density [mA/cm$^2$]")
lastCycleNumber = CVData['cycle number'].max()#-4 # For new settings file I use, last cycle should be analyzed.
CVLastSweep = CVData.loc[CVData['cycle number'] == lastCycleNumber].reset_index()
OxPotential, OxCurrentd, RedPotential, RedCurrentd = ele.get_ECSA_axis(CVLastSweep["Ewe-Ece/V"], CVLastSweep["<I>/mA"], HFR=0, H2crossover=0)



# Try to get capacitive baseline:
rimNum = 50 # Tells how far apart from the rim one should look for capacitive baseline point.
ReductiveSweepCurrentNoRims = RedCurrentd[rimNum:-rimNum]
ReductiveCapBaseline = np.max(ReductiveSweepCurrentNoRims)
ReductiveCapBIndex = np.argmax(ReductiveSweepCurrentNoRims)+rimNum
OxidativeSweepCurrentNoRims = OxCurrentd[50:-50]
OxidativeCapBaseline = np.min(OxidativeSweepCurrentNoRims)
OxidativeCapBIndex = np.argmin(OxidativeSweepCurrentNoRims)+rimNum
BaseLineShift = (ReductiveCapBaseline + OxidativeCapBaseline)/2
RedCurrentd -= BaseLineShift
OxCurrentd -= BaseLineShift
ReductiveCapBaseline -= BaseLineShift
OxidativeCapBaseline -= BaseLineShift
startVoltage = 0.062 # Manually change this value depending on what you analyze.
OxidativeStartIndex = np.argmin(abs(OxPotential-startVoltage))
ReductiveEndIndex = np.argmin(abs(RedPotential-startVoltage))
A_geo = 1.0 # Units cm2
plt.plot(RedPotential, RedCurrentd)
plt.plot(OxPotential, OxCurrentd, color="tab:blue")
plt.hlines(ReductiveCapBaseline, xmin = RedPotential[ReductiveEndIndex], xmax = RedPotential[ReductiveCapBIndex])
plt.hlines(OxidativeCapBaseline, xmin = OxPotential[OxidativeStartIndex], xmax = OxPotential[OxidativeCapBIndex], color="tab:blue")
plt.vlines(RedPotential[ReductiveEndIndex], ymin=RedCurrentd[ReductiveEndIndex], ymax=ReductiveCapBaseline)
plt.vlines(OxPotential[OxidativeStartIndex], ymin=OxidativeCapBaseline, ymax=OxCurrentd[OxidativeStartIndex], color="tab:blue")
plt.grid()
plt.show()
#print("Checks:")
#print(OxidativeCapBaseline)
#print(OxPotential[OxidativeStartIndex])
#print(OxPotential[OxidativeCapBIndex])
integralOx = integrate.simpson(OxCurrentd[OxidativeStartIndex:OxidativeCapBIndex]-OxidativeCapBaseline, OxPotential[OxidativeStartIndex:OxidativeCapBIndex])
integralRed = integrate.simpson(RedCurrentd[ReductiveCapBIndex:ReductiveEndIndex]-ReductiveCapBaseline, RedPotential[ReductiveCapBIndex:ReductiveEndIndex], -0.02)
#print(OxPotential[OxidativeStartIndex])
#print(RedPotential[ReductiveEndIndex])
print("The oxidative and reductive integrals are:")
print(integralOx)
print(integralRed)
integral = (abs(integralOx) + abs(integralRed))*A_geo/2 # Units: mVA
scanrate = 50 # Units: mV/s
Q = integral/scanrate # Units = As = C
specificCharge = 0.000210 # Units = C/cm2
A_Pt = Q/specificCharge # Units cm2
A_Pt = A_Pt/10000 # Units m2

Loading = 0.0003 # Units g/cm2
mass = Loading*A_geo # Units g

ECSA = A_Pt/mass # Units m2/g
print("The ECSA in m2/g is:")
print(ECSA) # 30.42
