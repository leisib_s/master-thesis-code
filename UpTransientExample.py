import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
# plt.rc('text', usetex=True)
# import scipy.integrate as integrate
import functions as ele


UpTransientFile1 = "Up Transient_70C_100RH_GDE_HP_GDE_hotpress_140C_10bar_01_CA_C01.mpt"
UpTransientFile2 = "Up Transient_70C_100RH_GDE_HP_GDE_hotpress_140C_10bar_02_CP_C01.mpt"
UpTransientFile3 = "Up Transient_70C_100RH_GDE_HP_GDE_hotpress_140C_10bar_03_CP_C01.mpt"
UpTransientFile4 = "Up Transient_70C_100RH_GDE_HP_GDE_hotpress_140C_10bar_04_OCV_C01.mpt"

UpTransientData1 = ele.open_mpt(UpTransientFile1, encoding = "iso8859-1")
UpTransientData2 = ele.open_mpt(UpTransientFile2, encoding = "iso8859-1")
UpTransientData3 = ele.open_mpt(UpTransientFile3, encoding = "iso8859-1")
UpTransientData4 = ele.open_mpt(UpTransientFile4, encoding = "iso8859-1")

#plt.title("Typical Up Transient Experiment")
plt.xlabel("Time [s]")
plt.ylabel("Potential [V]")
plt.xlim(xmax=350)
Potential1 = np.array(UpTransientData1["Ewe/V"])
Potential2 = np.array(UpTransientData2["Ewe/V"])
Potential3 = np.array(UpTransientData3["Ewe/V"])
Potential4 = np.array(UpTransientData4["Ewe/V"])
PotentialArray = np.concatenate([Potential1, Potential2, Potential3, Potential4])
Time1 = np.array(UpTransientData1["time/s"])
Time2 = np.array(UpTransientData2["time/s"])
Time3 = np.array(UpTransientData3["time/s"])
Time4 = np.array(UpTransientData4["time/s"])
TimeArray = np.concatenate([Time1, Time2, Time3, Time4])
plt.plot(Time1, Potential1, label="600 mV step")
plt.plot(Time2, Potential2, label="20 mA/cm$^2$ step")
plt.plot(Time3, Potential3, label="1000 mA/cm$^2$ step")
plt.plot(Time4, Potential4, label="OCV step")
plt.legend()
plt.grid()
plt.show()
