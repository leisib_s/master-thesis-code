import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
# plt.rc('text', usetex=True)
# import scipy.integrate as integrate
import functions as ele
# import sys
# import matplotlib.ticker as ticker
from scipy import stats

PlotPolarizationCurve = False
PlotTafelPlot = False
PlotMassActivity = False
PlotMassActivityBarplot = False
PlotSpecificActivity = False
PlotSpecificActivityBarplot = False
PlotHFRs = False
PlotH2crossovers = True
PlotUpTransient = False
PlotRsheet = False
PlotOverpoenialDeconvolution = False
PlotMTX = False


# All references at 80°C and 100% RH, for GDE/HP/GDE: ECSA = 58 m2/g

TempArray = np.array([40, 50, 60, 70, 80])
RHArray = np.array([40, 50, 60, 70, 80, 90, 100])

PlotTitleArray = np.array(["Aerogel", "Pt/C"])
PlotTitleArray = np.array(["GDE"])
#PlotTitleArray = np.array(["Aerogel 40% RH", "Aerogel 100% RH", "GDE 40% RH", "GDE 100% RH"])
#PlotTitleArray = np.array(["Aerogel 40°C", "Aerogel 80°C", "GDE 40°C", "GDE 80°C"])
TempSorted = False
RH_Sorted = True
if TempSorted:
    PlotTitleArray = np.array(["40°C", "50°C", "60°C", "70°C", "80°C"])
elif RH_Sorted:
    PlotTitleArray = np.array(["40% RH", "50% RH", "60% RH", "70% RH", "80% RH", "90% RH", "100% RH"])

plot_with_errors = True
MultiFile = True
labels_with_errors = np.array(["GDE", "Aerogel"])
PlotSebastian = False
colorArray = np.array(["tab:green", "tab:blue", "tab:red", "tab:orange"])
colorArray = np.array(["tab:blue", "tab:green", "gold", "tab:orange", "tab:red"])
colorArray = np.array(["tab:red", "tab:orange", "gold", "tab:green", "tab:cyan", "tab:blue", "tab:purple"])

plotO2Ref = False
Ref_O2_Current = np.array([5, 10, 15, 25, 35, 50, 100, 200, 500, 750, 1000, 1250, 1500])
Ref_O2_H2corrCurrentDensity = np.array([0.00301, 0.00785, 0.01289, 0.01788, 0.0279, 0.03785, 0.05285, 0.10272, 0.20285, 0.50031, 0.75051, 0.99991, 1.25023, 1.50043])
Ref_O2_Potential = np.array([0.93251, 0.91317, 0.90324, 0.88944, 0.88012, 0.8702, 0.84858, 0.82493, 0.78999, 0.7567, 0.73149, 0.70733, 0.68352])
Ref_O2_iRfreePot = np.array([1.0336, 0.93681, 0.92596, 0.90939, 0.89598, 0.88907, 0.88019, 0.86168, 0.84243, 0.81307, 0.79771, 0.78554, 0.77518, 0.76375])
Ref_O2_specific_activity = np.array([0.0000129741, 0.0000338362, 0.0000555603, 0.000077069, 0.000120259, 0.000163147, 0.000227802, 0.000442759, 0.000874353, 0.00216, 0.00323, 0.00431, 0.00539, 0.00647]) # Units A/cm2
plotAirRef = False
Ref_air_Current = np.array([5, 10, 15, 25, 35, 50, 100, 200, 500, 750, 1000, 1250, 1500])
Ref_air_H2corrCurrentDensity = np.array([0.003, 0.00789, 0.01289, 0.01787, 0.02777, 0.03785, 0.05272, 0.10284, 0.2027, 0.50122, 0.75131, 1.00124, 1.25144, 1.50168])
Ref_air_Potential = np.array([0.9095, 0.8922, 0.88244, 0.869, 0.859, 0.85, 0.826, 0.80229, 0.76, 0.716, 0.673, 0.62772, 0.573])
Ref_air_iRfreePot = np.array([0.989, 0.91344, 0.89427, 0.88374, 0.87063, 0.86124, 0.85239, 0.83219, 0.8104, 0.78649, 0.75027, 0.71607, 0.67237, 0.62042])
Ref_air_specific_activity = Ref_air_H2corrCurrentDensity/0.0004/58/10000 # 10000 is used for unit conversion, assume last GDE uses same GDE for air and oxygen.
plotAirRefAerogel = False # Here Pt3Ni_72 is used as the reference, because it's the reference with the best performance.
Ref_Pt3Ni_air_Current = np.array([0.00485, 0.00988, 0.01488, 0.02486, 0.03913, 0.04977, 0.09974, 0.19973, 0.4983, 0.74869, 0.99755, 1.2487, 1.49779])*1000
Ref_Pt3Ni_air_H2corrCurrentDensity = np.array([0.00355, 0.0084, 0.01343, 0.01843, 0.02841, 0.04268, 0.05332, 0.10329, 0.20328, 0.50185, 0.75224, 1.0011, 1.25225, 1.50134])
Ref_Pt3Ni_air_corrPotential = np.array([0.94188, 0.90837, 0.88768, 0.87728, 0.86392, 0.85502, 0.84433, 0.82353, 0.80186, 0.77806, 0.75454, 0.72007, 0.67484, 0.60915])
Ref_Pt3Ni_air_Potential = np.array([0.90805, 0.88703, 0.8763, 0.86228, 0.85245, 0.84107, 0.81699, 0.78883, 0.74742, 0.70865, 0.66184, 0.60147, 0.51689])

SebastianVoltage = np.array([[0.989, 0.9132, 0.8938, 0.883, 0.86939, 0.8595, 0.8499, 0.8272, 0.80041, 0.76108, 0.71211, 0.66516, 0.6087, 0.54099],
                             [0.9717, 0.89665, 0.87872, 0.8681, 0.8541, 0.84467, 0.83373, 0.81055, 0.7821, 0.7327, 0.68589, 0.62994, 0.537, 0.41816],
                             [0.9498, 0.9135, 0.8851, 0.8745, 0.86123, 0.85204, 0.83997, 0.82067, 0.7949, 0.75091, 0.71353, 0.67526, 0.63167, 0.58026]])
SebastianVoltageStd = stats.sem(SebastianVoltage, axis=0)
SebastianVoltage = np.mean(SebastianVoltage, axis=0)
SebastianCurrent = np.array([[0, 0.00489, 0.00989, 0.01487, 0.02477, 0.03485, 0.04972, 0.09984, 0.1997, 0.49822, 0.74831, 0.99824, 1.24844, 1.49868],
                             [0, 0.00494, 0.01001, 0.01492, 0.02495, 0.0349, 0.0499, 0.09982, 0.19981, 0.49911, 0.748, 0.99945, 1.24922, 1.49853],
                             [0, 0.00492, 0.00991, 0.01493, 0.02471, 0.03477, 0.04981, 0.09985, 0.19985, 0.49808, 0.74766, 0.99708, 1.24819, 1.49735]])*1000
SebastianCurrent = np.mean(SebastianCurrent, axis=0)
SebastianCorrCurrent = np.array([[0.003, 0.00789, 0.01289, 0.01787, 0.02777, 0.03785, 0.05272, 0.10284, 0.2027, 0.50122, 0.75131, 1.00124, 1.25144, 1.50168],
                                 [0.002, 0.00694, 0.01201, 0.01692, 0.02695, 0.0369, 0.0519, 0.10182, 0.20181, 0.50111, 0.75, 1.00145, 1.25122, 1.50053],
                                 [0.0032, 0.00812, 0.01311, 0.01813, 0.02791, 0.03797, 0.05301, 0.10305, 0.20305, 0.50128, 0.75086, 1.00028, 1.25139, 1.50055]])*1000
SebastianCorrCurrent = np.mean(SebastianCorrCurrent, axis=0)
SebastianCorrVoltage = np.array([[0.989, 0.91344, 0.89427, 0.88374, 0.87063, 0.86124, 0.85239, 0.83219, 0.8104, 0.78649, 0.75027, 0.71607, 0.67237, 0.62042],
                                 [0.9717, 0.89691, 0.87923, 0.86886, 0.85537, 0.84645, 0.83628, 0.81564, 0.79229, 0.75815, 0.72404, 0.67931, 0.60196, 0.49968],
                                 [0.9498, 0.91372, 0.88555, 0.87518, 0.86236, 0.85362, 0.84224, 0.82522, 0.80383, 0.77412, 0.74738, 0.71675, 0.69009, 0.65035]])
SebastianCorrVoltageStd = stats.sem(SebastianCorrVoltage, axis=0)
SebastianCorrVoltage = np.mean(SebastianCorrVoltage, axis=0)
# Note that this is in Oxygen not Air. All remaining Sebastian data are from air.
SebastianCorrVoltageO2 = np.array([[1.023, 0.94168, 0.92379, 0.91361, 0.90145, 0.89203, 0.88401, 0.8628, 0.84632, 0.82055, 0.80048, 0.78856, 0.78198, 0.7698],
                                   [1.03, 0.92998, 0.91287, 0.90237, 0.88995, 0.88028, 0.87084, 0.85099, 0.83558, 0.82163, 0.79988, 0.78125, 0.77262, 0.77222],
                                   [1.0258, 0.9328, 0.91373, 0.90413, 0.89084, 0.88205, 0.87293, 0.85397, 0.83563, 0.81778, 0.79733, 0.78583, 0.77454, 0.76409]])
SebastianCorrVoltageO2Std = stats.sem(SebastianCorrVoltageO2, axis=0)
#SebastianCorrVoltageO2 = np.mean(SebastianCorrVoltageO2, axis=0)
SebastianSpecificActivity = np.array([[5.60897E-6, 2.19872E-5, 3.82051E-5, 5.42308E-5, 8.57692E-5, 1.17885E-4, 1.65994E-4, 3.26122E-4, 6.46635E-4, 0.00161, 0.00241, 0.00321, 0.00401, 0.00481],
                                      [0.0, 2.9569E-5, 5.0819E-5, 7.25431E-5, 1.15086E-4, 1.57974E-4, 2.23233E-4, 4.38362E-4, 8.6931E-4, 0.00215, 0.00323, 0.00431, 0.00539, 0.00647],
                                      [1.34375E-5, 3.52232E-5, 5.77679E-5, 7.97768E-5, 1.24375E-4, 1.68393E-4, 2.35402E-4, 4.58705E-4, 9.05223E-4, 0.00224, 0.00335, 0.00447, 0.00559, 0.0067]])*1000*1000
SebAct900mV = np.empty(3)
for i in range(3):
    SebAct900mV[i] = ele.get_900mV(SebastianCorrVoltageO2[i], SebastianSpecificActivity[i])
SebActMean = np.mean(SebAct900mV)
SebActStd = stats.sem(SebAct900mV)

SebastianSpecificActivityStd = stats.sem(SebastianSpecificActivity, axis=0)
SebastianSpecificActivity = np.mean(SebastianSpecificActivity, axis=0)


"""PlotAerogelWithError = False
AerogelData1 = pd.read_csv("V0161_T11_RH100_80C_Air_V0161 (Aerogel).csv")
AerogelData2 = pd.read_csv("V0169_T11_RH100_80C_Air_V0169 (Aerogel).csv")
AerogelData3 = pd.read_csv("V0227_T15_RH100_80C_Air_V0227 (Aerogel).csv")
AerogelCurrent = np.array([AerogelData1["current_density"], AerogelData2["current_density"], AerogelData3["current_density"]])
AerogelCurrentMean = np.mean(AerogelCurrent, axis=0)
AerogelCurrentStd = np.std(AerogelCurrent, axis=0)
AerogelVoltage = np.array([AerogelData1["cell_voltages"], AerogelData2["cell_voltages"], AerogelData3["cell_voltages"]])
AerogelVoltageMean = np.mean(AerogelVoltage, axis=0)
AerogelVoltageStd = np.std(AerogelVoltage, axis=0)
AerogelH2corrj = np.array([AerogelData1["H2_corrected_j"], AerogelData2["H2_corrected_j"], AerogelData3["H2_corrected_j"]])
AerogelH2corrjMean = np.mean(AerogelH2corrj, axis=0)
AerogelH2corrjStd = np.std(AerogelH2corrj, axis=0)
AerogeliRcorrV = np.array([AerogelData1["iR_corrected_Voltage"], AerogelData2["iR_corrected_Voltage"], AerogelData3["iR_corrected_Voltage"]])
AerogeliRcorrVMean = np.mean(AerogeliRcorrV, axis=0)
AerogeliRcorrVStd = np.std(AerogeliRcorrV, axis=0)
AerogelSpecAct = np.array([AerogelData1["specific_activity"], AerogelData2["specific_activity"], AerogelData3["specific_activity"]])
AerogelSpecActMean = np.mean(AerogelSpecAct, axis=0)
AerogelSpecActStd = np.std(AerogelSpecAct, axis=0)
AerogelMassAct = np.array([AerogelData1["mass_activity"], AerogelData2["mass_activity"], AerogelData3["mass_activity"]])
AerogelMassActMean = np.mean(AerogelMassAct, axis=0)
AerogelMassActStd = np.std(AerogelMassAct, axis=0)"""



#files = np.array(["V0227_T15_RH040_80C_Air_40% RH.csv", "V0227_T15_RH050_80C_Air_50% RH.csv", "V0227_T15_RH060_80C_Air_60% RH.csv", "V0227_T15_RH070_80C_Air_70% RH.csv", "V0227_T15_RH080_80C_Air_80% RH.csv", "V0227_T15_RH090_80C_Air_90% RH.csv", "V0227_T15_RH100_80C_Air_100% RH.csv"])
#files = np.array(["V0161_T11_RH100_80C_Air_V0161.csv", "V0169_T11_RH100_80C_Air_V0169.csv", "V0227_T15_RH100_80C_Air_V0227.csv"])
#files = np.array(["V0169_T11_RH040_80C_Air_40% RH.csv", "V0169_T11_RH050_80C_Air_50% RH.csv", "V0169_T11_RH060_80C_Air_60% RH.csv", "V0169_T11_RH070_80C_Air_70% RH.csv", "V0169_T11_RH080_80C_Air_80% RH.csv", "V0169_T11_RH090_80C_Air_90% RH.csv", "V0169_T11_RH100_80C_Air_V0169.csv"])
#files = np.array(["V0227_T15_RH100_40C_Air_40°C.csv", "V0227_T15_RH100_50C_Air_50°C.csv", "V0227_T15_RH100_60C_Air_60°C.csv", "V0227_T15_RH100_70C_Air_70°C.csv", "V0227_T15_RH100_80C_Air_80°C.csv"])
#files = np.array(["V0161_T11_RH100_40C_Air_40°C.csv", "V0161_T11_RH100_50C_Air_50°C.csv", "V0161_T11_RH100_60C_Air_60°C.csv", "V0161_T11_RH100_70C_Air_70°C.csv", "V0161_T11_RH100_80C_Air_80°C.csv"])
#files = np.array(["V0227_T15_RH040_80C_Air_40% RH.csv", "V0227_T15_RH100_80C_Air_100% RH.csv"])
#files = np.array(["V0227_T15_RH100_40C_Air_40°C.csv", "V0227_T15_RH100_80C_Air_80°C.csv"])
files = [["V0161_T11_RH100_80C_Air_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_Air_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_Air_V0227 (Aerogel).csv"], ["V0158_T11_RH100_80C_Air_V0158 (GDE).csv", "V0229_T15_RH100_80C_Air_V0229 (GDE).csv"]]
files = [["V0161_T11_RH100_80C_Air_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_Air_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_Air_V0227 (Aerogel).csv"]]
files = [["V0161_T11_RH100_80C_O2_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_O2_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_O2_V0227 (Aerogel).csv"], ["V0161_T11_RH100_80C_O2_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_O2_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_O2_V0227 (Aerogel).csv"]]

Aerogel40RH = ["V0161_T11_RH040_80C_Air_40% RH.csv", "V0169_T11_RH040_80C_Air_40% RH.csv", "V0227_T15_RH040_80C_Air_40% RH.csv"]
Aerogel50RH = ["V0161_T11_RH050_80C_Air_50% RH.csv", "V0169_T11_RH050_80C_Air_50% RH.csv", "V0227_T15_RH050_80C_Air_50% RH.csv"]
Aerogel60RH = ["V0161_T11_RH060_80C_Air_60% RH.csv", "V0169_T11_RH060_80C_Air_60% RH.csv", "V0227_T15_RH060_80C_Air_60% RH.csv"]
Aerogel70RH = ["V0161_T11_RH070_80C_Air_70% RH.csv", "V0169_T11_RH070_80C_Air_70% RH.csv", "V0227_T15_RH070_80C_Air_70% RH.csv"]
Aerogel80RH = ["V0161_T11_RH080_80C_Air_80% RH.csv", "V0169_T11_RH080_80C_Air_80% RH.csv", "V0227_T15_RH080_80C_Air_80% RH.csv"]
Aerogel90RH = ["V0161_T11_RH090_80C_Air_90% RH.csv", "V0169_T11_RH090_80C_Air_90% RH.csv", "V0227_T15_RH090_80C_Air_90% RH.csv"]
Aerogel100RH = ["V0161_T11_RH100_80C_Air_100% RH.csv", "V0169_T11_RH100_80C_Air_100% RH.csv", "V0227_T15_RH100_80C_Air_100% RH.csv"]
files = [Aerogel40RH, Aerogel50RH, Aerogel60RH, Aerogel70RH, Aerogel80RH, Aerogel90RH, Aerogel100RH]
Aerogel40C = ["V0161_T11_RH100_40C_Air_40°C.csv", "V0169_T11_RH100_40C_Air_40°C.csv", "V0227_T15_RH100_40C_Air_40°C.csv"]
Aerogel50C = ["V0161_T11_RH100_50C_Air_50°C.csv", "V0169_T11_RH100_50C_Air_50°C.csv", "V0227_T15_RH100_50C_Air_50°C.csv"]
Aerogel40C = ["V0169_T11_RH100_40C_Air_40°C.csv", "V0227_T15_RH100_40C_Air_40°C.csv"]
Aerogel50C = ["V0169_T11_RH100_50C_Air_50°C.csv", "V0227_T15_RH100_50C_Air_50°C.csv"]

Aerogel60C = ["V0161_T11_RH100_60C_Air_60°C.csv", "V0169_T11_RH100_60C_Air_60°C.csv", "V0227_T15_RH100_60C_Air_60°C.csv"]
Aerogel70C = ["V0161_T11_RH100_70C_Air_70°C.csv", "V0169_T11_RH100_70C_Air_70°C.csv", "V0227_T15_RH100_70C_Air_70°C.csv"]
Aerogel80C = ["V0161_T11_RH100_80C_Air_80°C.csv", "V0169_T11_RH100_80C_Air_80°C.csv", "V0227_T15_RH100_80C_Air_80°C.csv"]
#files = [Aerogel40C, Aerogel50C, Aerogel60C, Aerogel70C, Aerogel80C]
#files = [["V0161_T11_RH100_80C_O2_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_O2_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_O2_V0227 (Aerogel).csv"]]
#files = [["V0227_T15_RH100_80C_O2_80°C.csv", "V0227_T15_RH100_80C_O2_80°C.csv", "V0227_T15_RH100_80C_O2_80°C.csv"]]
#files = [["V0230_T15_RH100_80C_O2_V0230 (GDE).csv", "V0230_T15_RH100_80C_O2_V0230 (GDE).csv", "V0230_T15_RH100_80C_O2_V0230 (GDE).csv"]]

GDE40RH = ["V0158_T11_RH040_80C_Air_40% RH.csv", "V0229_T15_RH040_80C_Air_40% RH.csv", "V0230_T15_RH040_80C_Air_40% RH.csv"]
GDE50RH = ["V0158_T11_RH050_80C_Air_50% RH.csv", "V0229_T15_RH050_80C_Air_50% RH.csv", "V0230_T15_RH050_80C_Air_50% RH.csv"]
GDE60RH = ["V0158_T11_RH060_80C_Air_60% RH.csv", "V0229_T15_RH060_80C_Air_60% RH.csv", "V0230_T15_RH060_80C_Air_60% RH.csv"]
GDE70RH = ["V0158_T11_RH070_80C_Air_70% RH.csv", "V0229_T15_RH070_80C_Air_70% RH.csv", "V0230_T15_RH070_80C_Air_70% RH.csv"]
GDE80RH = ["V0158_T11_RH080_80C_Air_80% RH.csv", "V0229_T15_RH080_80C_Air_80% RH.csv", "V0230_T15_RH080_80C_Air_80% RH.csv"]
GDE90RH = ["V0158_T11_RH090_80C_Air_90% RH.csv", "V0229_T15_RH090_80C_Air_90% RH.csv", "V0230_T15_RH090_80C_Air_90% RH.csv"]
GDE100RH = ["V0158_T11_RH100_80C_Air_100% RH.csv", "V0229_T15_RH100_80C_Air_100% RH.csv", "V0230_T15_RH100_80C_Air_100% RH.csv"]
"""GDE40RH = ["V0158_T11_RH040_80C_Air_40% RH.csv", "V0229_T15_RH040_80C_Air_40% RH.csv"]
GDE50RH = ["V0158_T11_RH050_80C_Air_50% RH.csv", "V0229_T15_RH050_80C_Air_50% RH.csv"]
GDE60RH = ["V0158_T11_RH060_80C_Air_60% RH.csv", "V0229_T15_RH060_80C_Air_60% RH.csv"]
GDE70RH = ["V0158_T11_RH070_80C_Air_70% RH.csv", "V0229_T15_RH070_80C_Air_70% RH.csv"]
GDE80RH = ["V0158_T11_RH080_80C_Air_80% RH.csv", "V0229_T15_RH080_80C_Air_80% RH.csv"]
GDE90RH = ["V0158_T11_RH090_80C_Air_90% RH.csv", "V0229_T15_RH090_80C_Air_90% RH.csv"]
GDE100RH = ["V0158_T11_RH100_80C_Air_100% RH.csv", "V0229_T15_RH100_80C_Air_100% RH.csv"]"""
files2 = [GDE40RH, GDE50RH, GDE60RH, GDE70RH, GDE80RH, GDE90RH, GDE100RH]
GDE40C = ["V0158_T11_RH100_40C_Air_40°C.csv", "V0229_T15_RH100_40C_Air_40°C.csv", "V0230_T15_RH100_40C_Air_40°C.csv"]
GDE50C = ["V0158_T11_RH100_50C_Air_50°C.csv", "V0229_T15_RH100_50C_Air_50°C.csv", "V0230_T15_RH100_50C_Air_50°C.csv"]
GDE60C = ["V0158_T11_RH100_60C_Air_60°C.csv", "V0229_T15_RH100_60C_Air_60°C.csv", "V0230_T15_RH100_60C_Air_60°C.csv"]
GDE70C = ["V0158_T11_RH100_70C_Air_70°C.csv", "V0229_T15_RH100_70C_Air_70°C.csv", "V0230_T15_RH100_70C_Air_70°C.csv"]
GDE80C = ["V0158_T11_RH100_80C_Air_80°C.csv", "V0229_T15_RH100_80C_Air_80°C.csv", "V0230_T15_RH100_80C_Air_80°C.csv"]
"""GDE40C = ["V0158_T11_RH100_40C_Air_40°C.csv", "V0229_T15_RH100_40C_Air_40°C.csv"]
GDE50C = ["V0158_T11_RH100_50C_Air_50°C.csv", "V0229_T15_RH100_50C_Air_50°C.csv"]
GDE60C = ["V0158_T11_RH100_60C_Air_60°C.csv", "V0229_T15_RH100_60C_Air_60°C.csv"]
GDE70C = ["V0158_T11_RH100_70C_Air_70°C.csv", "V0229_T15_RH100_70C_Air_70°C.csv"]
GDE80C = ["V0158_T11_RH100_80C_Air_80°C.csv", "V0229_T15_RH100_80C_Air_80°C.csv"]"""
#files2 = [GDE40C, GDE50C, GDE60C, GDE70C, GDE80C]
#files2 = [GDE40C, GDE50C, GDE60C, GDE70C, GDE100RH]
#files = files2
AerogelTest = ["V0161_T11_RH100_80C_Air_V0161 (Aerogel).csv", "V0169_T11_RH100_80C_Air_V0169 (Aerogel).csv", "V0227_T15_RH100_80C_Air_V0227 (Aerogel).csv"]
GDETest = ["V0158_T11_RH100_80C_Air_V0158 (GDE).csv", "V0229_T15_RH100_80C_Air_V0229 (GDE).csv", "V0230_T15_RH100_80C_Air_V0230 (GDE).csv"]
#files = [AerogelTest, Aerogel100RH, Aerogel80C, GDETest, GDE100RH, GDE80C]
#files = [Aerogel40RH, Aerogel100RH, GDE40RH, GDE100RH]
#files = [Aerogel40C, Aerogel80C, GDE40C, GDE80C]

#files2 = ["V0158_T11_RH040_80C_Air_40% RH.csv", "V0158_T11_RH050_80C_Air_50% RH.csv", "V0158_T11_RH060_80C_Air_60% RH.csv", "V0158_T11_RH070_80C_Air_70% RH.csv", "V0158_T11_RH080_80C_Air_80% RH.csv", "V0158_T11_RH090_80C_Air_90% RH.csv", "V0158_T11_RH100_80C_Air_100% RH.csv"]
#files2 = ["V0158_T11_RH100_40C_Air_40°C.csv", "V0158_T11_RH100_50C_Air_50°C.csv", "V0158_T11_RH100_60C_Air_60°C.csv", "V0158_T11_RH100_70C_Air_70°C.csv", "V0158_T11_RH100_80C_Air_80°C.csv"]
def getMeanArray(listOfString, attribute):
    counter = 0
    leastDataPoints = 100
    for x in listOfString:
        data = pd.read_csv(x)
        data = data[attribute]
        if len(data) < leastDataPoints:
            leastDataPoints = len(data)
        counter += 1
    DataList = np.zeros((counter, leastDataPoints))
    counter = 0
    for x in listOfString:
        data = pd.read_csv(x)
        DataList[counter] = data[attribute][:leastDataPoints]
        counter += 1
    return np.mean(DataList, axis=0)

def getMeanActivity(listOfString, attribute):
    counter = 0
    leastDataPoints = 100
    for x in listOfString:
        data = pd.read_csv(x)
        data = data[attribute]
        if len(data) < leastDataPoints:
            leastDataPoints = len(data)
        counter += 1
    DataList = np.zeros(counter)
    counter = 0
    for x in listOfString:
        data = pd.read_csv(x)
        DataList[counter] = ele.get_900mV(data["iR_corrected_Voltage"][:leastDataPoints], data[attribute][:leastDataPoints])
        counter += 1
    return np.mean(DataList)


def getStdArray(listOfString, attribute):
    counter = 0
    leastDataPoints = 100
    for x in listOfString:
        data = pd.read_csv(x)
        data = data[attribute]
        if len(data) < leastDataPoints:
            leastDataPoints = len(data)
        counter += 1
    DataList = np.zeros((counter, leastDataPoints))
    counter = 0
    for x in listOfString:
        data = pd.read_csv(x)
        DataList[counter] = data[attribute][:leastDataPoints]
        counter += 1
    return stats.sem(DataList, axis=0)

def getStdActivity(listOfString, attribute):
    counter = 0
    leastDataPoints = 100
    for x in listOfString:
        data = pd.read_csv(x)
        data = data[attribute]
        if len(data) < leastDataPoints:
            leastDataPoints = len(data)
        counter += 1
    DataList = np.zeros(counter)
    counter = 0
    for x in listOfString:
        data = pd.read_csv(x)
        DataList[counter] = ele.get_900mV(data["iR_corrected_Voltage"][:leastDataPoints], data[attribute][:leastDataPoints])
        counter += 1
    print("Compare:")
    print(np.std(DataList))
    print(stats.sem(DataList))
    return stats.sem(DataList)




# Polarization Curves
if PlotPolarizationCurve:
    #plt.title("Polarization Curve (80°C, 100% RH, H$_2$/Air, 1.7 bar)")
    #plt.title("Polarization Curve (100% RH, H$_2$/Air, 1.7 bar)")
    plt.text(1000, 0.93, "80°C, H$_2$/O$_2$, 1.7 bar", fontsize="large")
    plt.ylabel("Potential [V]")
    plt.xlabel("Current density [mA/cm$^2$]")
    if plotAirRef:
        label = "Reference Data"
        plt.plot(Ref_air_Current, Ref_air_Potential, label=label, marker='o')
    if plotO2Ref:
        label = "Reference Data"
        plt.plot(Ref_O2_Current, Ref_O2_Potential, label=label, marker='o')
    if plotAirRefAerogel:
        plt.plot(Ref_Pt3Ni_air_Current, Ref_Pt3Ni_air_Potential, label="Reference Data", marker='o')
    #if PlotAerogelWithError:
    #    plt.errorbar(AerogelCurrentMean, AerogelVoltageMean, yerr=AerogelVoltageStd, label="Aerogel", capsize=3)
    TitleCounter = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            plt.plot(data["current_density"], data["cell_voltages"], label=plotTitle, marker='o')
        else:
            CDdataMean = getMeanArray(x, "current_density")
            CVdataMean = getMeanArray(x, "cell_voltages")
            if plot_with_errors:
                # CDdataStd = getStdArray(x, "current_density")
                CVdataStd = getStdArray(x, "cell_voltages")
                plt.errorbar(CDdataMean, CVdataMean, yerr=CVdataStd, label=PlotTitleArray[TitleCounter], capsize=3, marker='o', linewidth=3, color=colorArray[TitleCounter], linestyle='--')
            else:
                plt.plot(CDdataMean, CVdataMean, label=PlotTitleArray[TitleCounter], marker='o')
            TitleCounter += 1
    if PlotSebastian:
        plt.errorbar(SebastianCurrent, SebastianVoltage, yerr=SebastianVoltageStd, label="GDE", capsize=3, marker='o', linewidth=3)
    plt.legend()
    plt.grid()
    plt.show()


# Tafel plot:
if PlotTafelPlot:
    #plt.title("Tafel Plot (100% RH, H$_2$/Air, 1.7 bar)")
    #plt.title("Tafel Plot (80°C, H$_2$/Air, 1.7 bar)")
    plt.xlabel("H$_2$-corrected Current Density [mA/cm$^2_{MEA}$]")
    plt.xlabel("H$_2$-corrected current density [$\mu$A/cm$^2_{Pt}$]")
    plt.ylabel("iR- and H$^+$ resistance-corrected potential [V]")
    plt.text(1000, 0.98, "80°C, H$_2$/O$_2$, 1.7 bar", fontsize="large")
    if PlotSebastian:
        plt.ylabel("iR-corrected potential [V]")
    plt.xscale(value="log")
    plt.xticks(ticks=[0.01, 0.1, 1, 10, 100, 1000, 10000], labels=["0.01", "0.1", "1", "10", "100", "1000", "10'000"])
    if plotAirRef:
        label = "Reference Data"
        plt.plot(Ref_air_H2corrCurrentDensity, Ref_air_iRfreePot, label=label, marker='o')
    if plotO2Ref:
        label = "Reference Data"
        plt.plot(Ref_O2_H2corrCurrentDensity, Ref_O2_iRfreePot, label=label, marker='o')
    if plotAirRefAerogel:
        plt.plot(Ref_Pt3Ni_air_H2corrCurrentDensity, Ref_Pt3Ni_air_corrPotential, label="Reference Data", marker='o')
    #if PlotAerogelWithError:
    #    plt.errorbar(AerogelH2corrjMean/1000, AerogeliRcorrVMean, yerr=AerogeliRcorrVStd, label="Aerogel", capsize=3)
    TitleCounter = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            plt.plot(data["H2_corrected_j"], data["iR_corrected_Voltage"], label=plotTitle, marker='o')
        else:
            CDdataMean = getMeanArray(x, "H2_corrected_j")
            if plot_with_errors:
                CVdataStd = getStdArray(x, "iR_corrected_Voltage")
                plt.errorbar(CDdataMean, CVdataMean, yerr=CVdataStd, label=PlotTitleArray[TitleCounter], capsize=3, marker='o', linewidth=3, color=colorArray[TitleCounter], linestyle='--')
            else:
                plt.plot(CDdataMean, CVdataMean, label=PlotTitleArray[TitleCounter], marker='o')
            TitleCounter += 1
    if plotAirRef or plotAirRefAerogel: # Stupid temporary fix
        intercept = 0.90 # Intercept for reference data
        plt.axline([10, intercept], [1000, intercept-0.14], linestyle='--', color='red', label="70 mV/dec linear fit")
    if plotO2Ref:
        intercept = 0.93 # Intercept for reference data
        plt.axline([0.01, intercept], [1, intercept-0.14], linestyle='--', color='green', label="70 mV/dec linear fit")
    intercept = 0.91 # Intercept for reference data
    if PlotSebastian:
        #plt.errorbar(SebastianCorrCurrent, SebastianCorrVoltage, yerr=SebastianCorrVoltageStd, label="GDE Sebastian", capsize=3, marker='x')
        plt.errorbar(SebastianSpecificActivity, SebastianCorrVoltageO2, yerr=SebastianCorrVoltageO2Std, label="GDE", capsize=3, marker='o', linewidth=3)
    plt.axline([100, intercept], [10000, intercept-0.14], linestyle='--', color='red', label="70 mV/dec linear fit")
    plt.legend(loc='lower left')
    plt.grid(True, which="both")
    plt.show()




# Mass activity barplot
if PlotMassActivityBarplot:
    #plt.title("Mass Activity at 0.9 V (80°C, H$_2$/Air, 1.7 bar)")
    #plt.title("Mass Activity at 0.9 V (100% RH, H$_2$/Air, 1.7 bar)")
    plt.text(38, 43, "100% RH, H$_2$/air, 1.7 bar", fontsize="large")
    plt.ylabel("Mass Activity [A/g]")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
    elif RH_Sorted:
        plt.xlabel("Relative Humidity [%]")
    xAxis = np.zeros(len(files))
    yAxis = np.zeros(len(files))
    yAxisError = np.zeros(len(files))
    i = 0
    TitleCounter = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            Experiment = x[:5]
            Testbench = x[6:9]
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                xAxis[i] = Temp
            elif RH_Sorted:
                xAxis[i] = RH
            yAxis[i] = ele.get_900mV(data["iR_corrected_Voltage"], data["mass_activity"])
            i += 1
        else:
            yAxis[i] = getMeanActivity(x, "mass_activity")
            if plot_with_errors:
                # CDdataStd = getStdArray(x, "current_density")
                yAxisError[i] = getStdActivity(x, "mass_activity")
                #plt.errorbar(CDdataMean, CVdataMean, yerr=CVdataStd, label=PlotTitleArray[TitleCounter], capsize=3)
            #else:
            #    plt.plot(CDdataMean, CVdataMean, label=PlotTitleArray[TitleCounter], marker='o')
            if TempSorted:
                xAxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                xAxis[i] = RHArray[TitleCounter]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        x = plt.bar(xAxis, yAxis, yerr=yAxisError, width=8, capsize = 8)
    else:
        x = plt.bar(xAxis, yAxis, width=8)
    #plt.errorbar(xAxis, yAxis, yerr=AerogelMassActStd)
    plt.bar_label(container=x, fmt='%.1f')
    plt.grid(axis='y')
    plt.show()

# Specific activity barplot
if PlotSpecificActivityBarplot:
    #plt.title("Specific Activity at 0.9 V (80°C, H$_2$/Air, 1.7 bar)")
    #plt.title("Specific Activity at 0.9 V (80°C, 100% RH, H$_2$/Air, 1.7 bar)")
    plt.ylabel("H$_2$-corrected current density [$\mu$A/cm$^2_{Pt}$]")
    plt.text(78, 95, "80°C, 100% RH, H$_2$/air, 1.7 bar", fontsize="large")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        plt.text(34, 141, "100% RH, H$_2$/air, 1.7 bar", fontsize="large")
        plt.ylim(ymax = 148)
    elif RH_Sorted:
        plt.xlabel("Relative humidity [%]")
        plt.text(80, 85, "80°C, H$_2$/air, 1.7 bar", fontsize="large")
    #plt.xlabel("")
    plt.xticks(ticks=[40, 50, 60, 70, 80, 90], labels=["Aerogel after\n conditioning", "Aerogel after RH\n measuremens", "Aerogel after RH and\n temp. measurements", "GDE after\n conditioning", "GDE after RH\n measurements", "GDE after RH and\n temp. measurements"])
    #plt.xticks(ticks=[10, 20], labels=["Aerogel","GDE"])
    xAxis = np.zeros(len(files))
    yAxis = np.zeros(len(files))
    yAxisError = np.zeros(len(files))
    i = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            Experiment = x[:5]
            Testbench = x[6:9]
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                xAxis[i] = Temp
            elif RH_Sorted:
                xAxis[i] = RH
            yAxis[i] = ele.get_900mV(data["iR_corrected_Voltage"], data["specific_activity"])
            i += 1
        else:
            yAxis[i] = getMeanActivity(x, "specific_activity")
            if plot_with_errors:
                # CDdataStd = getStdArray(x, "current_density")
                yAxisError[i] = getStdActivity(x, "specific_activity")
                #plt.errorbar(CDdataMean, CVdataMean, yerr=CVdataStd, label=PlotTitleArray[TitleCounter], capsize=3)
            #else:
            #    plt.plot(CDdataMean, CVdataMean, label=PlotTitleArray[TitleCounter], marker='o')
            if TempSorted:
                xAxis[i] = TempArray[i]
            if RH_Sorted:
                xAxis[i] = RHArray[i]
            i += 1
    xAxis = np.array(["Aerogel after conditioning", "Aerogel after RH measuremens", "Aerogel after RH and temp. measurements", "GDE after conditioning", "GDE after RH measurements", "GDE after RH and temp. measurements"])
    xAxis = np.array([40, 50, 60, 70, 80, 90])
    if plot_with_errors:
        x = plt.bar(xAxis, yAxis, yerr=yAxisError, width=8, capsize = 8)
    else:
        x = plt.bar(xAxis, yAxis, width=8)
    #plt.errorbar(xAxis, yAxis, yerr=AerogelMassActStd)
    plt.bar_label(container=x, fmt='%.1f')
    plt.grid(axis='y')
    plt.show()

# HFR plot
if PlotHFRs:
    #plt.title("HFR Plot (80°C, H$_2$/N$_2$, 1.7 bar)")
    plt.text(40.5, 0.05, "100% RH, H$_2$/air, 1.7 bar", fontsize="large")
    plt.ylabel("HFR [$\Omega$ cm$^2$]")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        plt.xticks(ticks=[40, 50, 60, 70, 80], labels=["40", "50", "60", "70", "80"])
    elif RH_Sorted:
        plt.xlabel("Relative Humidity [%]")
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    TitleCounter = 0
    i = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                plotXaxis[i] = Temp
            elif RH_Sorted:
                plotXaxis[i] = RH
            plotYaxis[i] = data["HFR"][0]
        else:
            #CDdataMean = getMeanArray(x, "current_density")
            plotYaxis[i] = getMeanArray(x, "HFR")[0]
            if plot_with_errors:
                plotYaxisError[i] = getStdArray(x, "HFR")[0]
            if TempSorted:
                plotXaxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                plotXaxis[i] = RHArray[TitleCounter]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="Aerogel", capsize=3, marker='o', linestyle='--')
    else:
        plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='o')
    TitleCounter = 0
    i = 0
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    if MultiFile:
        for x in files2:
            if isinstance(x, str):
                data = pd.read_csv(x)
                RH = int(x[12:15])
                Temp = int(x[16:18])
                Medium = x[20:22]
                plotTitle = x[23:-4]
                if Medium == "Ai":
                    Medium = x[20:23]
                    plotTitle = x[24:-4]
                if TempSorted:
                    plotXaxis[i] = Temp
                elif RH_Sorted:
                    plotXaxis[i] = RH
                    print(RH)
                plotYaxis[i] = data["HFR"][0]
                i += 1
            else:
                #CDdataMean = getMeanArray(x, "current_density")
                plotYaxis[i] = getMeanArray(x, "HFR")[0]
                if plot_with_errors:
                    plotYaxisError[i] = getStdArray(x, "HFR")[0]
                if TempSorted:
                    plotXaxis[i] = TempArray[TitleCounter]
                if RH_Sorted:
                    plotXaxis[i] = RHArray[TitleCounter]
                TitleCounter += 1
                i += 1
        if plot_with_errors:
            print(plotXaxis)
            print(plotYaxis)
            plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="GDE", capsize=3, marker='o', linestyle='--')
        else:
            plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='o')
    plt.legend()
    plt.grid()
    plt.show()

# H2crossover plot
if PlotH2crossovers:
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        #plt.title("H$_2$ Crossover vs Temperature (100% RH, H$_2$/N$_2$, 1.7 bar)")
        plt.text(40, 3.27, "100% RH, H$_2$/N$_2$, 1.7 bar", fontsize="large")
        plt.xticks(ticks=[40, 50, 60, 70, 80], labels=["40", "50", "60", "70", "80"])
    elif RH_Sorted:
        plt.xlabel("Relative humidity [%]")
        #plt.title("H$_2$ Crossover vs RH (80°C, H$_2$/N$_2$, 1.7 bar)")
        plt.text(37.3, 3.49, "80°C, H$_2$/N$_2$, 1.7 bar", fontsize="large")
    plt.ylabel("Current density [mA/cm$^2$]")
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    TitleCounter = 0
    i = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                plotXaxis[i] = Temp
            elif RH_Sorted:
                plotXaxis[i] = RH
            plotYaxis[i] = data["HFR"][0]
        else:
            #CDdataMean = getMeanArray(x, "current_density")
            plotYaxis[i] = getMeanArray(x, "H2_corrected_j")[0] - getMeanArray(x, "current_density")[0]
            if plot_with_errors:
                plotYaxisError[i] = getStdArray(x, "H2_corrected_j")[0] - getStdArray(x, "current_density")[0]
            if TempSorted:
                plotXaxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                plotXaxis[i] = RHArray[TitleCounter]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="Aerogel", capsize=3, marker='o', linestyle='--')
    else:
        plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='o')
    TitleCounter = 0
    i = 0
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    if MultiFile:
        for x in files2:
            if isinstance(x, str):
                data = pd.read_csv(x)
                RH = int(x[12:15])
                Temp = int(x[16:18])
                Medium = x[20:22]
                plotTitle = x[23:-4]
                if Medium == "Ai":
                    Medium = x[20:23]
                    plotTitle = x[24:-4]
                if TempSorted:
                    plotXaxis[i] = Temp
                elif RH_Sorted:
                    plotXaxis[i] = RH
                    print(RH)
                plotYaxis[i] = np.mean(data["H2_corrected_j"] - data["current_density"])
                i += 1
            else:
                #CDdataMean = getMeanArray(x, "current_density")
                plotYaxis[i] = getMeanArray(x, "H2_corrected_j")[0] - getMeanArray(x, "current_density")[0]
                if plot_with_errors:
                    plotYaxisError[i] = getStdArray(x, "H2_corrected_j")[0] - getStdArray(x, "current_density")[0]
                if TempSorted:
                    plotXaxis[i] = TempArray[TitleCounter]
                if RH_Sorted:
                    plotXaxis[i] = RHArray[TitleCounter]
                TitleCounter += 1
                i += 1
        if plot_with_errors:
            print(plotXaxis)
            print(plotYaxis)
            plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="GDE", capsize=3, marker='o', linestyle='--')
        else:
            plt.plot(plotXaxis, plotYaxis, label="GDE", marker='o')
    plt.legend()
    plt.grid()
    plt.show()

# Up transients plot
if PlotUpTransient:
    #plt.title("Up-Transient Plot (100% RH, H$_2$/Air, 1.7 bar)")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        plt.xticks(ticks=[40, 50, 60, 70, 80], labels=["40", "50", "60", "70", "80"])
        plt.text(61, 0.41, "100% RH, H$_2$/Air, 1.7 bar", fontsize="large")
    elif RH_Sorted:
        plt.xlabel("Relative humidity [%]")
        plt.text(75, 0.51, "80°C, H$_2$/Air, 1.7 bar", fontsize="large")
    plt.ylabel("Minimum potential [V]")
    plt.ylim(-0.05, 0.69)
    plotYaxis = np.zeros(len(files))
    #plotYaxisOneAmp = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    TitleCounter = 0
    i = 0
    #j = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                plotXaxis[i] = Temp
            elif RH_Sorted:
                plotXaxis[i] = RH
            plotYaxis[i] = data["HFR"][0]
        else:
            #j += 1
            #if j < 3:
            #   continue
            #CDdataMean = getMeanArray(x, "current_density")
            plotYaxis[i] = getMeanArray(x, "Up_Transient_Voltage")[0]
            if plot_with_errors:
                plotYaxisError[i] = getStdArray(x, "Up_Transient_Voltage")[0]
            if TempSorted:
                plotXaxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                plotXaxis[i] = RHArray[TitleCounter]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="Aerogel", capsize=3, marker = 'o', linestyle='--')
    else:
        plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='x', linestyle='None')
    TitleCounter = 0
    i = 0
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    if MultiFile:
        for x in files2:
            if isinstance(x, str):
                data = pd.read_csv(x)
                RH = int(x[12:15])
                Temp = int(x[16:18])
                Medium = x[20:22]
                plotTitle = x[23:-4]
                if Medium == "Ai":
                    Medium = x[20:23]
                    plotTitle = x[24:-4]
                if TempSorted:
                    plotXaxis[i] = Temp
                elif RH_Sorted:
                    plotXaxis[i] = RH
                    print(RH)
                plotYaxis[i] = data["Up_Transient_Voltage"][0]
                print(plotYaxis[i])
                i += 1
            else:
                #CDdataMean = getMeanArray(x, "current_density")
                plotYaxis[i] = getMeanArray(x, "Up_Transient_Voltage")[0]
                if plot_with_errors:
                    plotYaxisError[i] = getStdArray(x, "Up_Transient_Voltage")[0]
                if TempSorted:
                    plotXaxis[i] = TempArray[TitleCounter]
                if RH_Sorted:
                    plotXaxis[i] = RHArray[TitleCounter]
                TitleCounter += 1
                i += 1
        if plot_with_errors:
            plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="GDE", capsize=3, marker = 'o', linestyle='--')
        else:
            plt.plot(plotXaxis, plotYaxis, label="GDE (1000 mA, Up-Transient)", marker='x', linestyle='None')
    plt.axline([40, 0], [80, 0], linestyle='--', label="Failure below this line", color="red")
    plt.plot([40, 50], [-0.05, -0.05], linestyle='None', marker='o', color="C0")
    plt.ylim(-0.1, 0.7)
    plt.grid()
    plt.legend()
    plt.show()

if PlotRsheet:
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        #plt.title("R$_{sheet}$ vs Temperature (100% RH, H$_2$/N$_2$, 1.7 bar)")
        #plt.xticks(ticks=[40, 50, 60, 70, 80], labels=["40", "50", "60", "70", "80"])
        plt.text(75, 0.08, "80°C, H$_2$/air, 1.7 bar", fontsize="large")
    elif RH_Sorted:
        plt.xlabel("Relative humidity [%]")
        plt.text(80, 0.075, "80°C, H$_2$/air, 1.7 bar", fontsize="large")
    plt.ylabel("R$_{sheet}$  [Ω cm$^2$]")
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    TitleCounter = 0
    i = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                plotXaxis[i] = Temp
            elif RH_Sorted:
                plotXaxis[i] = RH
            plotYaxis[i] = data["HFR"][0]
        else:
            #CDdataMean = getMeanArray(x, "current_density")
            plotYaxis[i] = getMeanArray(x, "R_sheet")[0]
            if plot_with_errors:
                plotYaxisError[i] = getStdArray(x, "R_sheet")[0]
            if TempSorted:
                plotXaxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                plotXaxis[i] = RHArray[TitleCounter]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="Aerogel", capsize=3, marker='o', linestyle='--')
    else:
        plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='o')
    TitleCounter = 0
    i = 0
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    if MultiFile:
        for x in files2:
            if isinstance(x, str):
                data = pd.read_csv(x)
                RH = int(x[12:15])
                Temp = int(x[16:18])
                Medium = x[20:22]
                plotTitle = x[23:-4]
                if Medium == "Ai":
                    Medium = x[20:23]
                    plotTitle = x[24:-4]
                if TempSorted:
                    plotXaxis[i] = Temp
                elif RH_Sorted:
                    plotXaxis[i] = RH
                    print(RH)
                plotYaxis[i] = data["R_sheet"][0]
                i += 1
            else:
                #CDdataMean = getMeanArray(x, "current_density")
                plotYaxis[i] = getMeanArray(x, "R_sheet")[0]
                if plot_with_errors:
                    plotYaxisError[i] = getStdArray(x, "R_sheet")[0]
                if TempSorted:
                    plotXaxis[i] = TempArray[TitleCounter]
                if RH_Sorted:
                    plotXaxis[i] = RHArray[TitleCounter]
                TitleCounter += 1
                i += 1
        if plot_with_errors:
            print(plotXaxis)
            print(plotYaxis)
            plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="GDE", capsize=3, marker='o', linestyle='--')
        else:
            plt.plot(plotXaxis, plotYaxis, label="GDE", marker='o')
    plt.legend()
    plt.grid()
    plt.show()

if PlotOverpoenialDeconvolution:
    plt.title("Overpotenial deconvolution")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
    elif RH_Sorted:
        plt.xlabel("Relative Humidity [%]")
    plt.ylabel("Overpotenial [mV]")
    plotY_kinetic = np.zeros(len(files))
    plotY_ohmic = np.zeros(len(files))
    plotY_mass_transport = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    i=0
    for x in files:
        data = pd.read_csv(x)
        Experiment = x[:5]
        Testbench = x[6:9]
        RH = int(x[12:15])
        Temp = int(x[16:18])
        Medium = x[20:22]
        plotTitle = x[23:-4]
        if Medium == "Ai":
            Medium = x[20:23]
            plotTitle = x[24:-4]
        if TempSorted:
            plotXaxis[i] = Temp
        elif RH_sorted:
            plotXaxis[i] = RH
        plotY_kinetic[i] = np.array(data["kinetic_overpotential"])[-1]
        plotY_ohmic[i] = np.array(data["ohmic_overpotential"])[-1]
        plotY_mass_transport[i] = np.array(data["mass_transport_overpotential"])[-1]
        i += 1
    plt.plot(plotXaxis, plotY_kinetic, label = "Kinetic Overpotential")
    plt.plot(plotXaxis, plotY_ohmic, bottom=data["kinetic_overpotential"], label = "Ohmic Overpotential")
    plt.plot(plotXaxis, plotY_mass_transport, bottom=data["mass_transport_overpotential"], label = "Mass Transport Overpotential")
    plt.grid()
    plt.legend()
    plt.show()


if PlotMTX:
    #plt.title("Mass Transport Overpotential at 1250 mA (100% RH, H$_2$/Air, 1.7 bar)")
    plt.ylabel("$\eta_{Mass Transport}$ [V]")
    if TempSorted:
        plt.xlabel("Temperature [$\degree$C]")
        plt.xticks(ticks=[40, 50, 60, 70, 80], labels=["40", "50", "60", "70", "80"])
        plt.text(41, 0.141, "100% RH, H$_2$/air, 1.7 bar", fontsize="large")
    elif RH_Sorted:
        plt.xlabel("Relative humidity [%]")
        plt.text(77, 0.15, "80°C, H$_2$/air, 1.7 bar", fontsize="large")
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    TitleCounter = 0
    i = 0
    for x in files:
        if isinstance(x, str):
            data = pd.read_csv(x)
            RH = int(x[12:15])
            Temp = int(x[16:18])
            Medium = x[20:22]
            plotTitle = x[23:-4]
            if Medium == "Ai":
                Medium = x[20:23]
                plotTitle = x[24:-4]
            if TempSorted:
                plotXaxis[i] = Temp
            elif RH_Sorted:
                plotXaxis[i] = RH
            plotYaxis[i] = data["mass_transport_overpotential"][-2]
        else:
            #CDdataMean = getMeanArray(x, "current_density")
            plotYaxis[i] = getMeanArray(x, "mass_transport_overpotential")[-2]
            if plot_with_errors:
                plotYaxisError[i] = getStdArray(x, "mass_transport_overpotential")[-2]
            if TempSorted:
                plotXaxis[i] = TempArray[TitleCounter]
            if RH_Sorted:
                plotXaxis[i] = RHArray[TitleCounter]
            if i == 0:
                plotYaxis[i] = getMeanArray(x, "mass_transport_overpotential")[-1]
                plotYaxisError[i] = getStdArray(x, "mass_transport_overpotential")[-1]
            TitleCounter += 1
            i += 1
    if plot_with_errors:
        plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="Aerogel", capsize=3, marker='o', linestyle='--')
    else:
        plt.plot(plotXaxis, plotYaxis, label="Aerogel", marker='o')
    TitleCounter = 0
    i = 0
    plotYaxis = np.zeros(len(files))
    plotYaxisError = np.zeros(len(files))
    plotXaxis = np.zeros(len(files))
    if MultiFile:
        for x in files2:
            if isinstance(x, str):
                data = pd.read_csv(x)
                RH = int(x[12:15])
                Temp = int(x[16:18])
                Medium = x[20:22]
                plotTitle = x[23:-4]
                if Medium == "Ai":
                    Medium = x[20:23]
                    plotTitle = x[24:-4]
                if TempSorted:
                    plotXaxis[i] = Temp
                elif RH_Sorted:
                    plotXaxis[i] = RH
                    print(RH)
                plotYaxis[i] = data["mass_transport_overpotential"][-2]
                i += 1
            else:
                #CDdataMean = getMeanArray(x, "current_density")
                plotYaxis[i] = getMeanArray(x, "mass_transport_overpotential")[-2]
                if plot_with_errors:
                    plotYaxisError[i] = getStdArray(x, "mass_transport_overpotential")[-2]
                if TempSorted:
                    plotXaxis[i] = TempArray[TitleCounter]
                if RH_Sorted:
                    plotXaxis[i] = RHArray[TitleCounter]
                TitleCounter += 1
                i += 1
        if plot_with_errors:
            print(plotXaxis)
            print(plotYaxis)
            plt.errorbar(plotXaxis, plotYaxis, yerr=plotYaxisError, label="GDE", capsize=3, marker='o', linestyle='--')
        else:
            plt.plot(plotXaxis, plotYaxis, label="GDE", marker='o')
    plt.legend()
    plt.grid()
    plt.show()
