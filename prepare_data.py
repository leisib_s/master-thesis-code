import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
# plt.rc('text', usetex=True)
import functions as ele
from sklearn.linear_model import LinearRegression

# Calculate corrected current densities/potentials and save results as files.
# These can be read and plotted with plotter.py.
# Don't forget to adjust ECSA, loading and Rsheet!
PlotHFRs = True
PlotPotentials = True
PlotH2Crossover = True
PlotOverpotentials = True

UseMPT = True
UseTXT = not UseMPT



plotTitle = "80°C"
# plotTitle = "100% RH"
# plotTitle = "V0227 (Aerogel)"
Experiment = "V0227"
Testbench = "T15"
RH = 100
Temp = 80
# Use "Air" or "O2"
Medium = "O2"
Title = Medium + ", RH = " + str(RH) + "%, T = " + str(Temp) + "°C"
file_path = ""
file_ending_txt = ".txt"
file_ending_mpt = ".mpt"
if UseTXT:
    file_ending = file_ending_txt
elif UseMPT:
    file_ending = file_ending_mpt
file_starter = file_path + "IV low O2 80C_100RH_GDE_HP_Pt3Ni_124" # low current
OCV_file = file_starter + "_01_OCV_C01" + file_ending
CC_5mA = file_starter + "_02_CP_C01" + file_ending_txt
GEIS_5mA = file_starter + "_03_GEIS_C01" + file_ending
CC_10mA = file_starter + "_04_CP_C01" + file_ending_txt
GEIS_10mA = file_starter + "_05_GEIS_C01" + file_ending
CC_15mA = file_starter + "_06_CP_C01" + file_ending_txt
GEIS_15mA = file_starter + "_07_GEIS_C01" + file_ending
CC_25mA = file_starter + "_08_CP_C01" + file_ending_txt
GEIS_25mA = file_starter + "_09_GEIS_C01" + file_ending
CC_35mA = file_starter + "_10_CP_C01" + file_ending_txt
GEIS_35mA = file_starter + "_11_GEIS_C01" + file_ending
CC_50mA = file_starter + "_12_CP_C01" + file_ending_txt
GEIS_50mA = file_starter + "_13_GEIS_C01" + file_ending
CC_100mA = file_starter + "_14_CP_C01" + file_ending_txt
GEIS_100mA = file_starter + "_15_GEIS_C01" + file_ending
CC_200mA = file_starter + "_16_CP_C01" + file_ending_txt
GEIS_200mA = file_starter + "_17_GEIS_C01" + file_ending
# Ignore these 500 mA files for now, as the potential tends to be lower.
# CC_500mA = file_starter + "_18_CP_C01.txt"
# GEIS_500mA = file_starter + "_19_GEIS_C01.txt"
file_starter = file_path + "IV high O2 80C_100RH_GDE_HP_Pt3Ni_124" # high current
CC_500mA = file_starter + "_02_CP_C01" + file_ending_txt
GEIS_500mA = file_starter + "_03_GEIS_C01" + file_ending
CC_750mA = file_starter + "_04_CP_C01" + file_ending_txt
GEIS_750mA = file_starter + "_05_GEIS_C01" + file_ending
CC_1000mA = file_starter + "_06_CP_C01" + file_ending_txt
GEIS_1000mA = file_starter + "_07_GEIS_C01" + file_ending
CC_1250mA = file_starter + "_08_CP_C01" + file_ending_txt
GEIS_1250mA = file_starter + "_09_GEIS_C01" + file_ending
CC_1500mA = file_starter + "_10_CP_C01" + file_ending_txt
GEIS_1500mA = file_starter + "_11_GEIS_C01" + file_ending
crossoverFile = file_path + "80C/H2crossover_80C_100RH_GDE_HP_Pt3Ni_124_02_CVA_C01.mpt"
UpTransientFile = file_path + "Up transient after conditioning_GDE_HP_Pt3Ni_124_03_CP_C01.mpt"
PEISfile = file_path + "80C/PEIS_80C_100RH_GDE_HP_Pt3Ni_124_C01.mpt"
Rsheet = 0.06 # Insert value obtained from getRsheet.py here.
ECSA = 33.06 # Obtain from ECSA.py, units m2/g.
loading = 0.0003 # Units g/cm2
A_geo = 1 # Units cm2


# Only used temporarily, later updated to whatever is read from files.
CurrentDensities = np.array([0, 5.0, 10, 15, 25, 35, 50, 100, 200, 500, 750, 1000, 1250, 1500]) # Units mA/cm2
# Remove data for current densities, where there was a failure.
Fails = np.full(13, True)
FailsLong = np.full(len(CurrentDensities), True)
#Fails[-1] = False
FailsLong[1:] = Fails
CurrentDensities = CurrentDensities[FailsLong]

##### No need to modify anything below this #####



# Read out HFRs and check plot if they look ok.
GEIS_array = np.array([GEIS_5mA, GEIS_10mA, GEIS_15mA, GEIS_25mA, GEIS_35mA, GEIS_50mA, GEIS_100mA, GEIS_200mA, GEIS_500mA, GEIS_750mA, GEIS_1000mA, GEIS_1250mA, GEIS_1500mA])
GEIS_array = GEIS_array[Fails]
HFRs = np.zeros(len(GEIS_array)+1) # +1 allows writing into csv file. HFR at OCV is set to PEIS HFR.
plt.title("Impedance Spectra")
plt.xlabel(r"Re(Z) [$\Omega$ cm$^2$]")
plt.ylabel(r"-Im(Z) [$\Omega$ cm$^2$]")
for x in range(len(GEIS_array)):
    if UseTXT:
        HFRdata = pd.read_csv(GEIS_array[x], delimiter='\t', skiprows=[], encoding = "iso8859-1")
    elif UseMPT:
        HFRdata = ele.open_mpt(GEIS_array[x], encoding = "iso8859-1")
    HFRs[x+1] = ele.get_HFR(HFRdata["-Im(Z)/Ohm"], HFRdata["Re(Z)/Ohm"])
    if PlotHFRs:
        plt.plot(HFRdata["Re(Z)/Ohm"], HFRdata["-Im(Z)/Ohm"], label=str(CurrentDensities[x+1]) + " mA/cm$^2$")
if PlotHFRs:
    plt.legend()
    plt.grid()
    plt.show()
#UseTXT = True
if UseTXT:
    PEISdata = pd.read_csv(PEISfile, delimiter='\t', skiprows=[], encoding = "iso8859-1")
elif UseMPT:
    PEISdata = ele.open_mpt(PEISfile, encoding = "iso8859-1")
#UseTXT = False
PEISdata = PEISdata[ele.get_last_imp_cycle(PEISdata["freq/Hz"]):]
HFRs[0] = ele.get_HFR(PEISdata["-Im(Z)/Ohm"], PEISdata["Re(Z)/Ohm"])
HFRs = HFRs * A_geo # Units now Ωcm2



# Read out potentials and check plot if they look ok.
CC_array = np.array([CC_5mA, CC_10mA, CC_15mA, CC_25mA, CC_35mA, CC_50mA, CC_100mA, CC_200mA, CC_500mA, CC_750mA, CC_1000mA, CC_1250mA, CC_1500mA])
CC_array = CC_array[Fails]
CellVoltages = np.zeros(len(HFRs))
plt.title(Title)
plt.xlabel("Time [s]")
plt.ylabel("Cell Voltage [V]")
for x in range(len(GEIS_array)):
    if UseTXT:
        CC_data = pd.read_csv(CC_array[x], delimiter='\t', skiprows=[], encoding = "iso8859-1")
    elif UseMPT:
        CC_data = pd.read_csv(CC_array[x], delimiter='\t', skiprows=[], encoding = "iso8859-1")
        # CC_data = ele.open_mpt(CC_array[x], encoding = "iso8859-1") There seems to be a bug in EC-Lab or the settings file used.
    time = np.array(CC_data["time/s"])
    # Take average of last 50s, where we omit the last 10 seconds. (Use 40s of datapoints.)
    last_time_datapoint = time[-1]
    startIndex = np.argmin(abs(last_time_datapoint-50-time))
    endIndex = np.argmin(abs(last_time_datapoint-10-time))
    CellVoltages[x+1] = np.mean(np.array(CC_data["<Ewe>/V"])[startIndex:endIndex])
    CurrentDensities[x+1] = abs(np.mean(np.array(CC_data["I/mA"])[startIndex:endIndex]))
    if PlotPotentials:
        plt.plot(time, CC_data["<Ewe>/V"], label=str(CurrentDensities[x+1]) + " mA/cm$^2$")
if PlotPotentials:
    plt.legend()
    plt.grid()
    plt.show()

if UseTXT:
        OCVdata = pd.read_csv(OCV_file, delimiter='\t', skiprows=[], encoding = "iso8859-1")
elif UseMPT:
        OCVdata = ele.open_mpt(OCV_file, encoding = "iso8859-1")
CellVoltages[0] = np.mean(np.array(OCVdata["Ewe/V"])[5:-2])



# Cathode resistance correction
if Temp == 80:
    Tafelslope = 70 # Units mV/dec
elif Temp == 70:
    Tafelslope = 68
elif Temp == 60:
    Tafelslope = 66
elif Temp == 50:
    Tafelslope = 64
elif Temp == 40:
    Tafelslope = 62
Rsheet -= HFRs[0]
iRsheet = CurrentDensities/1000 * Rsheet/(Tafelslope/1000)
#print(iRsheet)
xi = 27.52481-27.52835*np.power(0.9744, iRsheet)
#print(xi)
iRHeffective = Rsheet/(3+xi)*CurrentDensities/1000
#print("IRHplusEffective is:")
#print(iRHeffective)
iRcorrVoltages = CellVoltages + CurrentDensities/1000 * HFRs + iRHeffective


# H2 crossover correction
#UseTXT=True
if UseTXT:
    crossoverData = pd.read_csv(crossoverFile, delimiter='\t', skiprows=[], encoding = "iso8859-1")
elif UseMPT:
    crossoverData = ele.open_mpt(crossoverFile, encoding = "iso8859-1")
#UseTXT=False
CellVoltage = np.array(crossoverData["Ewe/V"])
H2crossover = ele.get_H2crossover(crossoverData["<I>/mA"], CellVoltage)
if PlotH2Crossover:
    plt.title("H2 Crossover Current Density = %1.3f mA/cm$^2$" % H2crossover)
    plt.xlabel("Cell Voltage [V]")
    plt.ylabel("Current Density [mA/cm$^2$]")
    plt.plot(CellVoltage, crossoverData["<I>/mA"])
    plt.grid()
    plt.show()
H2corrCurrentDensities = CurrentDensities + H2crossover
# Turns out it should be added, as the H2crossover current is negative and needs to be compensated.
# The actual current is the one we see + the part used to compensate for the H2crossover current.
H2crossoverArray = np.full(len(CellVoltages), H2crossover)



# print("Start Calculating Mass Transport Overpotential.")
# Calcuate mass-transport overpotential (Method 1&2):
# 1. Do linear fit to get slope and intercept of Tafel slope. (Method 1 only esimate intercept, method 2 additionally estimae slope by linear regression.)
# 2. Use this information to calculate what current one would expect at each j.
# 3. Calculate its difference to obtain mass-transport overpotential.
Xfit = H2corrCurrentDensities[2:7]
Xfit = Xfit.reshape(-1,1)
Yfit = iRcorrVoltages[2:7]
reg = LinearRegression().fit(np.log10(Xfit), Yfit)
Intercept = np.mean(Yfit + Tafelslope/1000 * np.log10(Xfit)) # Guessed, linear regression estimate with known slope and only estimating intercept.
#print("Linear Regression Stuff")
#print(Yfit + Tafelslope * np.log10(Xfit.flatten()))
#print(Intercept)
#print(reg.intercept_)
#print(reg.coef_)
#x = np.array([10, 100, 1000])
#x = np.log10(x)
#print(reg.predict(x.reshape(-1,1)))
#mask = ~np.isnan(H2corrCurrentDensities)
TafelLine2 = reg.predict(np.log10(np.reshape(H2corrCurrentDensities,(-1, 1))))
TafelLine = Intercept - Tafelslope/1000 * np.log10(H2corrCurrentDensities)
mtx = TafelLine - iRcorrVoltages
mtx[mtx < 0] = 0
#print("Things that matter:")
#print(TafelLine)
#print(mtx)
mtx2 = TafelLine2 - iRcorrVoltages
mtx2[mtx2 < 0] = 0



# Calculate mass-transport overpotentials (Method 3):
# 1. Calculate Erev at 80°C
# 2. Substract HFR iR, sheet iR, kinetic overpotential and measured potential.
# 3. Whatever is remaining should be mtx3.
if Temp == 80: # TODO: add pressures for other temperatures
    pH2O_sat = 0.4736 # Units bar, taken from https://www.sugarprocesstech.com/vapour-pressure-water/
elif Temp == 70:
    pH2O_sat = 0.3116
elif Temp == 60:
    pH2O_sat = 0.1992
elif Temp == 50:
    pH2O_sat = 0.12335
elif Temp == 40:
    pH2O_sat = 0.07375
pH2 = 1.7-pH2O_sat # Substract water pressure
pO2 = pH2
if Medium == "Air":
    pO2 *= 0.21
Erev = 1.23 - 0.0009*(Temp-25) + 2.303*8.314*(Temp+273)/4/96485*np.log10(pH2*pH2*pO2/(RH/100)/(RH/100)) # Isn't one RH missing?
#print("Erev=")
#print(Erev)
CDs = H2corrCurrentDensities[1:] # crossover corrected current densities without OCV CD
# Tafel line: 0.91 - (0.07*log10(CurrentDensities))
#reg.intercept = 0.98
#No_kin_potential = reg.intercept_ + (reg.coef_*np.log10(CDs))
No_kin_potential = Intercept - Tafelslope/1000 * np.log10(CDs)
eta_kin = Erev - No_kin_potential
eta_ohmic = HFRs[1:] * CDs/1000 + iRHeffective[1:]
#print(HFRs)
mtx3 = Erev - CellVoltages[1:] - eta_kin - eta_ohmic
mtx3[mtx3 < 0] = 0
#print("Kinetic overpotentials")
#print(eta_kin) # mass transport overpotential
#print("Ohmic overpotentials")
#print(eta_ohmic) # mass transport overpotential
#print("Mass-transport overpotentials")
#print(eta_mass) # mass transport overpotential
#print("Cell Voltages:")
#print(CellVoltages)
#print("iR corrected voltages are:")
#print(iRcorrVoltages)
if PlotOverpotentials:
    plt.title("Overpotential Plot")
    plt.xlabel("Current Density [mA/cm$^2$]")
    plt.ylabel("Potential [V]")
    #print(mask)
    #print(len(CDs[mask]))
    #print(len(mtx[1:]))
    plt.plot(CDs, mtx[1:], label="Mass-Transport Overpotential")
    plt.plot(CDs, mtx2[1:], label="Mass-Transport Overpotential 2")
    plt.plot(CDs, mtx3, label="Mass-Transport Overpotential 3")
    plt.plot(CDs, eta_kin, label="Kinetic Overpotential")
    plt.plot(CDs, eta_ohmic, label="Ohmic Overpotential")
    plt.legend()
    plt.grid()
    plt.show()

# Up-transient
if UseTXT:
    UpTransientData = pd.read_csv(UpTransientFile, delimiter='\t', skiprows=[], encoding = "iso8859-1")
elif UseMPT:
    UpTransientData = ele.open_mpt(UpTransientFile, encoding = "iso8859-1")
# Maybe not the smartest way to do this but should work for now.
UpTransient = np.zeros(len(CellVoltages))
UpTransient[0] = np.mean(UpTransientData["Ewe/V"][-10:])

# Rsheet
RsheetArray = np.full(len(CellVoltages), Rsheet)

# Save Data
mass_activity = H2corrCurrentDensities / loading /1000 # Units A/g
specific_activity = mass_activity / ECSA * 100 # Units microA/cm2
eta_kin_new = np.zeros(len(iRcorrVoltages))
eta_ohmic_new = np.zeros(len(iRcorrVoltages))
mtx3_new = np.zeros(len(iRcorrVoltages))
eta_kin_new[1:] = eta_kin
eta_ohmic_new[1:] = eta_ohmic
mtx3_new[1:] = mtx3
"""print(len(CellVoltages))
print(len(iRcorrVoltages))
print(len(CurrentDensities))
print(len(H2corrCurrentDensities))
print(len(HFRs))
print(len(mass_activity))
print(len(specific_activity))
print(len(eta_kin_new))
print(len(eta_ohmic_new))
print(len(mtx))
print(len(mtx2))
print(len(mtx3_new))
print(len(UpTransient))
print(len(H2crossoverArray))
print(len(RsheetArray))"""
data = np.array([CellVoltages, iRcorrVoltages, CurrentDensities, H2corrCurrentDensities, HFRs, mass_activity, specific_activity, eta_kin_new, eta_ohmic_new, mtx, mtx2, mtx3_new, UpTransient, H2crossoverArray, RsheetArray])
data = pd.DataFrame(np.transpose(data), 
                    columns=['cell_voltages', 'iR_corrected_Voltage', 'current_density', 'H2_corrected_j', 'HFR', 'mass_activity', 'specific_activity', 'kinetic_overpotential', 'ohmic overpotential', 'mass_transport_overpotential', 'mass_transport_overpotential_2', 'mass_transport_overpotential_3', "Up_Transient_Voltage", 'H2_crossover', 'R_sheet'])
print(data.head())
if RH == 100:
    filename = Experiment + '_' + Testbench + "_" + "RH100_" + str(Temp) + "C_" + Medium + "_" + plotTitle + ".csv"
else:
    filename = Experiment + '_' + Testbench + "_" + "RH0" + str(RH) + "_" + str(Temp) + "C_" + Medium + "_" + plotTitle + ".csv"
data.to_csv(filename, index=False)


