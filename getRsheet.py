import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (9,8)
import scipy.integrate as integrate
import functions as ele
import sys
UseTXT = False
UseMPT = True
HFRfile80 = "PEIS after conditioning_GDE_HP_Pt3Ni_124_C01.txt"
HFRfile80 = "ECSA after RH measurements_GDE_HP_Pt3Ni_124_03_PEIS_C01.mpt"
#HFRfile80 = "RH100/PEIS 80C_100RH_GDE_HP_Pt3Ni_124_C01.mpt"
if UseTXT:
    HFRdata80 = pd.read_csv(HFRfile80, delimiter='\t', skiprows=[], encoding = "iso8859-1")
elif UseMPT:
    HFRdata80 = ele.open_mpt(HFRfile80, encoding = "iso8859-1")
# Last cycle is measured at 0.2 V according to settings file.
plt.xlim(0,0.3)
plt.ylim(0,0.3)
plt.xlabel("Re(Z) [$\Omega$ cm$^2$]")
plt.ylabel("-Im(Z) [$\Omega$ cm$^2$]")
HFRdata80 = HFRdata80[ele.get_last_imp_cycle(HFRdata80["freq/Hz"]):]
HFR80 = ele.get_HFR(HFRdata80["-Im(Z)/Ohm"], HFRdata80["Re(Z)/Ohm"])
plt.plot(HFRdata80["Re(Z)/Ohm"], HFRdata80["-Im(Z)/Ohm"], label = "100% RH (GDE)", color='red')
plt.axline([0+HFR80, 0], [0.1+HFR80, 0.1], linestyle='--', color='red', label='45$\degree$ line')
plt.gca().set_aspect('equal', adjustable='box')
plt.grid()
plt.legend()
plt.show()
