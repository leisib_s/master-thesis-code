import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
import matplotlib.pyplot as plt
import functions as ele
from scipy import stats
from sklearn.linear_model import LinearRegression

Temp = np.array([40, 50, 60, 70, 80])
OneByK = 1/(Temp+273.15)
R = 8.31446


# Procedure:
# 1. Calculate E_rev for all 5 temperatures.
# 2. Then use polarization curve to obtain current density at 0.35 V overpotential.
# 3. Correct by that dubious formula in slide 11 of Meriem's presentation.
# 4. Liner regression from Arrhenius plot.

# Linear regression reference:
# https://stackoverflow.com/questions/22381497/python-scikit-learn-linear-model-parameter-standard-error



# 1
pH2O_sat = np.array([0.07375, 0.12335, 0.1992, 0.3116, 0.4736])
pH2 = 1.7-pH2O_sat # Substract water pressure
pO2 = pH2
pO2 *= 0.21 # Medium == air
Erev = 1.23 - 0.0009*(Temp-25) + 2.303*8.314*(Temp+273.15)/4/96485*np.log10(pH2/1.01325*pH2/1.01325*pO2/1.01325) # Equation 2, Neyerlin
print(Erev-0.35)

#i_350mV_overpotential = np.array([])
"""filename = "V0229_T15_RH100_80C_Air_80°C.csv"
plt.title("Polarization Curve (100% RH, H$_2$/Air, 1.7 bar)")
plt.ylabel("Potential [V]")
plt.xlabel("Current Density [mA/cm$^2$]")
data = pd.read_csv(filename)
TempToPlot = int(filename[16:18])
index = int(TempToPlot/10-4)
plt.plot(data["current_density"], data["cell_voltages"], marker='o')
plt.axhline(y=Erev[index]-0.35, linestyle='-')
plt.grid()
plt.show()"""

GDE_350mV_overpotential = np.array([[68.502, 97.807, 98.357, 184.25, 241.83],
                                    [64.603, 94.569, 130.224, 184.679, 251.95],
                                    [74.779, 93.65, 120.435, 183.52, 232.89]])

# Order V0161, V0169, V0227, from 40 to 80, here in mA
i_350mV_overpotential = np.array([[62.44, 72.035, 130.328, 159.465, 190.497],
                         [45.607, 72.192, 93.698, 113.551, 143.416],
                         [33.981, 74.631, 106.979, 153.169, 188.62]])
i_350mV_overpotential #/= 1000
i_350mV_overpotential *= np.power(pO2, 0.54)
i_350mV_overpotentialSem = stats.sem(i_350mV_overpotential, axis=0)
i_350mV_overpotential = np.mean(i_350mV_overpotential, axis=0)
#print(np.power(pO2, 0.54))
#print(i_350mV_overpotential)

reg = LinearRegression().fit(OneByK.reshape(-1, 1), np.log(i_350mV_overpotential))
print("The activation energy in kJ/mol is:")
print(reg.coef_*R/1000)

X_with_intercept = np.empty(shape=(5, 2), dtype=np.float)
X_with_intercept[:,0]=1
X_with_intercept[:,1] = OneByK
beta_hat = np.linalg.inv(X_with_intercept.T @ X_with_intercept) @ X_with_intercept.T @ np.log(i_350mV_overpotential)

y_hat = reg.predict(OneByK.reshape(-1, 1))
residuals = np.log(i_350mV_overpotential) - y_hat
residual_sum_of_squares = residuals.T @ residuals
sigma_squared_hat = residual_sum_of_squares / (5 - 2)
var_beta_hat = np.linalg.inv(X_with_intercept.T @ X_with_intercept) * sigma_squared_hat
for p_ in range(2):
    standard_error = var_beta_hat[p_, p_] ** 0.5
    standard_error *= R/1000
    print(f"SE(beta_hat[{p_}]): {standard_error}")
print(beta_hat)
print("Results")
print(reg.coef_*R/1000)
#print(ols_result.summary())

#X = pd.DataFrame(OneByK, columns=['OneByK'])
#y = pd.DataFrame(np.log(i_350mV_overpotential), columns=['j'])        



#plt.title("Arrhenius Plot (100% RH, H$_2$/Air, 1.7 bar)")
plt.text(0.00305, 70, "100% RH, H$_2$/Air, 1.7 bar", fontsize="large")
plt.xlabel("1/T [1/K]")
plt.ylabel("Current Density at $\eta$ = 0.35 V in log scale [mA/cm$^2$]")
plt.yscale(value="log")
plt.yticks(ticks=[30, 40, 50, 60, 70, 80, 90, 100, 110, 120], labels=["30", "40", "50", "60", "70", "80", "90", "100", "110", "120"])
#plt.xticks(ticks=[0.00285, 0.00295, 0.00305, 0.00315], labels=["0.00285", "0.00295", "0.00305", "0.00315"])
#plt.plot(OneByK, i_350mV_overpotential, marker = 'o')
plt.errorbar(OneByK, i_350mV_overpotential, yerr=i_350mV_overpotentialSem, label="Aerogel", capsize=3, marker='o')
plt.grid()
plt.axline([OneByK[-1], np.exp(float(reg.intercept_) + OneByK[-1]*float(reg.coef_))], [OneByK[0], np.exp(float(reg.intercept_) + OneByK[0]*float(reg.coef_))], linestyle='--', color='red', label="Linear Fit Aerogel")


GDE_350mV_overpotential *= np.power(pO2, 0.54)
GDE_350mV_overpotentialSem = stats.sem(GDE_350mV_overpotential, axis=0)
GDE_350mV_overpotential = np.mean(GDE_350mV_overpotential, axis=0)
reg = LinearRegression().fit(OneByK.reshape(-1, 1), np.log(GDE_350mV_overpotential))
print("The activation energy of GDE in kJ/mol is:")
print(reg.coef_*R/1000)
X_with_intercept = np.empty(shape=(5, 2), dtype=np.float)
X_with_intercept[:,0]=1
X_with_intercept[:,1] = OneByK
beta_hat = np.linalg.inv(X_with_intercept.T @ X_with_intercept) @ X_with_intercept.T @ np.log(GDE_350mV_overpotential)

y_hat = reg.predict(OneByK.reshape(-1, 1))
residuals = np.log(GDE_350mV_overpotential) - y_hat
residual_sum_of_squares = residuals.T @ residuals
sigma_squared_hat = residual_sum_of_squares / (5 - 2)
var_beta_hat = np.linalg.inv(X_with_intercept.T @ X_with_intercept) * sigma_squared_hat
for p_ in range(2):
    standard_error = var_beta_hat[p_, p_] ** 0.5
    standard_error *= R/1000
    print(f"SE(beta_hat[{p_}]): {standard_error}")
print(beta_hat)
print("Results")
print(reg.coef_*R/1000)




plt.errorbar(OneByK, GDE_350mV_overpotential, yerr=GDE_350mV_overpotentialSem, label="GDE", capsize=3, marker='o')
plt.axline([OneByK[-1], np.exp(float(reg.intercept_) + OneByK[-1]*float(reg.coef_))], [OneByK[0], np.exp(float(reg.intercept_) + OneByK[0]*float(reg.coef_))], linestyle='--', color='green', label="Linear Fit GDE")
plt.legend()
plt.show()
